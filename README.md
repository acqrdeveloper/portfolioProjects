# Portafolio - Galeria de Proyectos

Galeria de proyectos actualizados

- 2018
    - [Encuesta Virtual](#encuesta-virtual)
    - [Portal de Atención](#portal-de-autoatención)
- 2017
    - [Gmail](#gmail)
    - [Rollyroll](#rollyroll)
    - [Sumate al Exito](#sumate-al-exito)
- 2016
    - [Centros Virtuales Administrador](#centros-virtuales-administrador)

## Encuesta Virtual

<span>Aplicación web, desarrollado para gestionar una encuesta a nivel corporativo en linea, lograr tambien capacitar, calificar y tener una estadística de ello.</span>

<table style="width:100%">
    <tr>
        <td width=25%><img src="2018/web_encuesta_virtual/login.png"/></td>
        <td width=25%><img src="2018/web_encuesta_virtual/themes.png"/></td>
        <td width=25%><img src="2018/web_encuesta_virtual/exam.png"/></td>
        <td width=25%><img src="2018/web_encuesta_virtual/questions.png"/></td>
    </tr>
</table>

<hr/>

## Portal de Autoatención

<span>Aplicación web, desarrollado para la integración con Servicios y Micro-Servicios Active Directory, Mensajeria, Base de Datos Logs.</span>

<table style="width:100%">
    <tr>
        <td width=25%><img src="2018/web_portal_autoatencion/search.png"/></td>
        <td width=25%><img src="2018/web_portal_autoatencion/error.png"/></td>
        <td width=25%><img src="2018/web_portal_autoatencion/home.png"/></td>
        <td width=25%><img src="2018/web_portal_autoatencion/paso_1.png"/></td>
    </tr>
</table>

<hr/>

## Gmail

<span>Aplicación movil hibrida, ejemplo de software para gestionar mensajeria en memoria cache.</span>

<table style="width:100%">
    <tr>
        <td width=25%><img src="2017/app_gmail/buscar.png"/></td>
        <td width=25%><img src="2017/app_gmail/configuracion.png"/></td>
        <td width=25%><img src="2017/app_gmail/crear.png"/></td>
        <td width=25%><img src="2017/app_gmail/general.png"/></td>
    </tr>
</table>

<hr/>

## Rollyroll

<span>Aplicación movil hibrida, software para gestionar ventas con pasarela de pago.</span>

<table style="width:100%">
    <tr>
        <td width=25%><img src="2017/app_rollyroll/principal.png"/></td>
        <td width=25%><img src="2017/app_rollyroll/detalle.png"/></td>
        <td width=25%><img src="2017/app_rollyroll/cesta_de_compra.png"/></td>
        <td width=25%><img src="2017/app_rollyroll/formas_de_pago.png"/></td>
    </tr>
</table>

<hr/>

## Centros Virtuales Administrador

<span>Aplicación web, software gestor adminitrativo del cliente app y web, cubre la facturación, reservaciones, reseteos y configuración interna del negocio.</span>

<table style="width:100%">
    <tr>
        <td width=25%><img src="2016/web_centros_virtuales/facturacion_cv.png"/></td>
        <td width=25%><img src="2016/web_centros_virtuales/home_cv.png"/></td>
        <td width=25%><img src="2016/web_centros_virtuales/login_cv.png"/></td>
        <td width=25%><img src="2016/web_centros_virtuales/reserva_cv.png"/></td>
    </tr>
</table>

<hr/>

## Sumate al Exito

<span>Aplicación web, <b>ex Centros Virtuales Cliente</b> es un software gestor cliente para el alquiler de oficinas.</span>

<table style="width:100%">
    <tr>
        <td width=25%><img src="2017/web_sumate/login.png"/></td>
        <td width=25%><img src="2017/web_sumate/reserva.png"/></td>
        <td width=25%><img src="2017/web_sumate/nueva_reserva.png"/></td>
        <td width=25%><img src="2017/web_sumate/bandeja.png"/></td>
    </tr>
</table>